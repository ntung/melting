# MELTING

This program computes, for a nucleic acid duplex, the enthalpy, the entropy and the melting temperature of the helix-coil transitions. Three types of hybridisation are possible: DNA/DNA, DNA/RNA, and RNA/RNA. The program first computes the hybridisation enthalpy and entropy (by the nearest-neighbor method by default but other models can be implemented). Then the melting temperature is computed. The set of thermodynamic parameters can be easely changed, for instance based on new experimental measurements. MELTING< 4 is written in ISO C and MELTING 5 in Java. It can be compiled on any operating system. Some perl scripts are provided for MELTING 4 to show how melting can be used as a block to construct more ambitious tools. MELTING 5 is way more than version 4, and can be used to develop more sophisticated computing tools.

MELTING is a Free Software, licenced under GPLv2. Its source code is open, and you can get it for free. MELTING is **NO LONGER DEVELOPED** by its original authors. However, feel free to download the code and do whatever you want with it.

## MELTING 5
The current stable version is v5.2. MELTING 5 has been rewritten from scratch in Java (1.5) and provides a large set of thermodynamic models to compute the enthalpy and entropy of several structures in the duplex: perfectly matching sequences, single mismatch, tandem mismatch, internal loop, single dangling end, second dangling end, long dangling end (only one to four poly A), single bulge loop, long bulge loop, inosine base (I), hydroxyadenine (A*), azobenzene (cis X_C and trans X_T), locked nucleic acids (Al, Gl, Tl, Cl). MELTING 5 can manage a fourth type of hybridization : 2-o-methyl RNA/RNA. A lot of new ion corrections are available for Sodium, Magnesium, Potassium, Tris concentrations. New approximative formulas are available and to take into account the magnesium, tris and potassium concentrations. Some formulas are also available to correct the melting temperature when DMSO and formamide are present. The MELTING predictions accuracy has been much improved in the 5 version. See the [user manual](https://gitlab.com/ngamblen/melting/-/blob/main/MELTING5.2.0/doc/melting.pdf) for more information. MELTING 5 can also be extended with new thermodynamic models. See the [Developer guide](https://gitlab.com/ngamblen/melting/-/blob/main/MELTING5.2.0/doc/DeveloppersGuide.pdf).


- The [MELTING 4.3.1 distribution](https://gitlab.com/ngamblen/melting/-/blob/main/Distributions/melting4_3_1.tar.gz) contains the program sources, binaries for Linux 32 bits, Linux 64 bits, Windows and MacOS, a GUI interface written in Perl/Tk, a perl script to run MELTING on multiple sequences, another to run iteratively <small>MELTING</small> along a nucleic acid by sliding a window of a specified width, the full documentation in several formats (PDF, HTML), a bunch of thermodynamic data tables and a benchmark of experimentally measured Tm.
- The [MELTING 5.2 distribution](https://gitlab.com/ngamblen/melting/-/blob/main/Distributions/MELTING5.2.0.tar.gz?ref_type=heads)	contains the JAR file of the program with some scripts to run directly the program on unix and Windows systems, the java source code, the Javadoc, the full documentation in several formats (PDF, HTML, PS, TXT, DVI), a bunch of thermodynamic data tables and examples of computed melting temperature in comparison to experimentally measured Tm.
- MELTING is licensed under the [GNU General Public License](http://www.gnu.org/licenses/licenses.html#TOCGPL). That limits your right to restrict the rights of others! In particular:
    - If you distribute MELTING, you have to distribute its sources as well, or add a reference to the present web-site in your documentation.
    - If you use MELTING source-code in a program, this program HAS TO BE released under the GNU General Public License or a compatible licence.
    - If you use MELTING as an invisible module of your program, behind a common interface, this program HAS TO BE released under the GNU General Public License or a compatible licence.
- [Debian package](http://packages.debian.org/source/melting)
- [Ubuntu package](http://packages.ubuntu.com/source/melting)
- [melting5jars](https://github.com/hrbrmstr/melting5jars), An R package wrapper for MELTING 5 jars

## Cite this software tool

Le Novère N. (2001). MELTING, computing the melting temperature of nucleic acid duplex. _Bioinformatics_, 17: 1226-1227. [doi:10.1093/bioinformatics/17.12.1226](https://doi.org/10.1093/bioinformatics/17.12.1226)

Dumousseau M., Rodriguez N., Juty N., Le Novère N. (2012) MELTING, a flexible platform to predict the melting temperatures of nucleic acids. _BMC Bioinformatics_, 13: 101. [doi:10.1186/1471-2105-13-101](https://doi.org/10.1186/1471-2105-13-101">)

## Software tools using MELTING

- [OpenPrimeR](http://openprimer.mpi-inf.mpg.de/)
- [OligoDB](http://oligodb.charite.de) _discontinued_ 
- [SOL](http://www.bionet.espci.fr/solhelp.html) _discontinued_
- [SEPON](http://www.agrsci.dk/hag/sepon/) _discontinued_
- [siRNA](http://bioinfo.wistar.upenn.edu/siRNA/siRNA.htm) _discontinued_

In addition, MELTING formed the basis of the [BioPython MeltingTemp package](http://biopython.org/DIST/docs/api/Bio.SeqUtils.MeltingTemp-module.html)

Tell-us if we missed any.

## authors
Nicolas Gambardella, Marine Dumousseau, Nicolas Rodriguez, Piero Dalle Pezze, W John Gowers.

